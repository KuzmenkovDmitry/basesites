<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Авторизация</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
	<script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>

<body>


	<div class="navbar navbar-inverse navbar-static-top tabbable">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#responsive-menu">
					<span class="sr-only">Открыть навигацию</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse nav navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="authorization">Авторизация</a></li>
					<li><a href="registration">Регистрация</a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:url value="/j_spring_security_check" var="login"/>
	<form method="post" action="${login}" class="form-horizontal">
		<div class = "form-group">
			<label class = "control-label col-lg-2">Email:</label>
			<div class = "col-lg-3">
				<input type="text" name = "j_email" class="form-control" placeholder = "Введите Email"/>
			</div>
		</div>
		<div class = "form-group">
			<label class = "control-label col-lg-2">Пароль:</label>
			<div class = "col-lg-3">
				<input type = "password" name = "j_password" class="form-control" placeholder = "Введите пароль"/>
			</div>
		</div>
		<div class = "form-group">
			<div class = "col-lg-offset-2 col-lg-3">
				<input type="submit" class = "btn btn-default" value="Войти"/>
			</div>
		</div>
	</form>

</body>
</html>