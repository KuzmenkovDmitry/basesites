<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Хранилище сайтов</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resourses/css/bootstrap.min.css" />
	<script src="${pageContext.request.contextPath}/resourses/js/jquery-1.11.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/resourses/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="navbar navbar-inverse navbar-static-top tabbable">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#responsive-menu">
					<span class="sr-only">Открыть навигацию</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse nav navbar-collapse" id="responsive-menu">
				<ul class="nav navbar-nav">
					<li><a href="addSites">Добавить ссылку</a></li>
					<li><a href="showSites">Посмотреть мои ссылки</a></li>
				</ul>
				<form action = "/searchSites" object="${Site}" method = "POST" class = "navbar-form navbar-right">
					<input type = "hidden" name = "command" value = "search"/>
					<div class = "form-group">
						<input type = "text" name="tag" field="*tag" class = "form-control" placeholder="Введите тег" valaue = ""/>
					</div>
					<input type = "submit" class = "btn btn-primary" value = "Искать"/>	
				</form>
			</div>
		</div>
	</div>

	<div class = "container-fluid">
		<div class = "row">
			<form class="form-horizontal" role="form" method="POST"
				action="/addSites" object="${Site}">
				<div class="form-group">
					<label class="control-label col-lg-2">Ссылка:</label>
					<div class="col-lg-3">
						<input type="text" required="required" class="form-control"
							name="link" field="*link" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Тег:</label>
					<div class="col-lg-3">
						<input type="text" required="required" class="form-control"
							name="tag" field="*tag"/>
					</div>
				</div>
				<div class="form-group">
					<div class=" col-lg-offset-2 col-lg-2">
						<input type="submit" class=" form-control btn btn-default"
							value="Добавить" />
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>