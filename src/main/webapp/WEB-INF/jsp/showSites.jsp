<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Хранилище сайтов</title>
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/resourses/css/bootstrap.min.css" />
	<script src="${pageContext.request.contextPath}/resourses/js/jquery-1.11.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/resourses/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="navbar navbar-inverse navbar-static-top tabbable">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#responsive-menu">
					<span class="sr-only">Открыть навигацию</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse nav navbar-collapse" id="responsive-menu">
				<ul class="nav navbar-nav">
					<li><a href="addSites">Добавить ссылку</a></li>
					<li><a href="baseSites?command=Index">Посмотреть мои ссылки</a></li>	
				</ul>
				<form action = "baseSites" method = "POST" class = "navbar-form navbar-right">
					<input type = "hidden" name = "command" value = "search"/>
					<div class = "form-group">
						<input type = "text" class = "form-control" placeholder="Введите тег" valaue = ""/>
					</div>
					<input type = "submit" class = "btn btn-primary" value = "Искать"/>	
				</form>
			</div>
		</div>
	</div>
	${user.email}
	<div class = "container-fluid">
		<div class = "row">
			<div class = "col-lg-10 col-lg-offset-1">
				<table class = "table table-striped">
					<tr>
						<th></th>
						<th>Тэг</th>
						<th>Ссылка</th>
					</tr>
					<c:forEach var="list" items="${sites}">
						<tr>
							<td></td>
							<td>
								<option value = "${list.tag}">${list.tag}</option>
							</td>
							<td>
								<a href=${list.link}>${list.link}</a>
							</td>
						</tr>
					</c:forEach>
				</table>	
			</div>
		</div>
	</div>

</body>
</html>