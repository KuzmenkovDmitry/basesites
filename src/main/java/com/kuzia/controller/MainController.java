package com.kuzia.controller;

import com.kuzia.model.Site;
import com.kuzia.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView start() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("addSites");
        modelAndView.addObject("site", new Site());
        return modelAndView;
    }

    @RequestMapping(value = "/showSites", method = RequestMethod.GET)
    public ModelAndView showSites() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("sites", userService.showSites());
        modelAndView.setViewName("showSites");
        return modelAndView;
    }

    @RequestMapping(value = "/addSites", method = RequestMethod.POST)
    public String addSites(@ModelAttribute Site site) {
        userService.addNewSite(site);
        System.out.println(site.getLink());
        return "addSites";
    }

    @RequestMapping(value = "/addSites", method = RequestMethod.GET)
    public ModelAndView toAddSites() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("site", new Site());
        modelAndView.setViewName("addSites");
        return modelAndView;
    }

    @RequestMapping(value = "/searchSites", method = RequestMethod.POST)
    public ModelAndView searchSites(@ModelAttribute Site site) {
        System.out.println("tag - " + site.getTag());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("sites", userService.searchSites(site.getTag()));
        modelAndView.setViewName("showSites");
        return modelAndView;
    }
}
