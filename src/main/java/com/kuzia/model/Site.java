package com.kuzia.model;

import javax.persistence.*;

@Entity
@Table(name = "sites")
public class Site {

	@Id
	@Column(name = "link")
	private String link;
	@Column(name = "tag")
	private String tag;
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_email")
	private User user;
	
	public Site() {}

	public Site(String link, String tag) {
		super();
		this.link = link;
		this.tag = tag;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
