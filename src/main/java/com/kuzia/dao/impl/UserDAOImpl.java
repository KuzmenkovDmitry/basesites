package com.kuzia.dao.impl;

import com.kuzia.dao.UserDAO;
import com.kuzia.model.Site;
import com.kuzia.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User getUserByEmail(String email) {
		return (User) sessionFactory.openSession()
				.createQuery("from User where email = ?")
				.setString(0, email)
				.list()
				.get(0);
	}

	@Override
	public void addNewSite(Site site) {
		sessionFactory.getCurrentSession().save(site);
	}

}
