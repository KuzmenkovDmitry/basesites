package com.kuzia.dao;

import com.kuzia.model.Site;
import com.kuzia.model.User;

public interface UserDAO {

	public User getUserByEmail(String email);

	public void addNewSite(Site site);
}
