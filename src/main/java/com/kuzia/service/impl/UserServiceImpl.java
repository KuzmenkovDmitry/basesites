package com.kuzia.service.impl;

import com.kuzia.dao.UserDAO;
import com.kuzia.model.Site;
import com.kuzia.model.User;
import com.kuzia.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public void addNewSite(Site site) {
        User user = getUser();
        site.setUser(user);
        userDAO.addNewSite(site);
    }

    @Override
    public List<Site> showSites() {
        User user = getUser();
        return user.getSites();
    }

    @Override
    public List<Site> searchSites(String tag) {
        User user = getUser();
        List<Site> sites = user.getSites();
        List<Site> searchSites = sites.stream().filter(site -> tag.equals(site.getTag())).collect(Collectors.toList());
        return searchSites;
    }

    private User getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();
        return userDAO.getUserByEmail(email);
    }
}
