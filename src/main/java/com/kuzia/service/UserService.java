package com.kuzia.service;


import com.kuzia.model.Site;
import com.kuzia.model.User;

import java.util.List;

public interface UserService {

    public void addNewSite(Site site);

    public List<Site> showSites();

    public List<Site> searchSites(String tag);

}
